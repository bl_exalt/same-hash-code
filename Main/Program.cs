﻿char[] printables = Enumerable.
    Range(32, 126 - 32 + 1).
    Select(x => (char)x).
    ToArray();

int maxStringLength = 10;
char[] buffer = new char[maxStringLength];

var hashCodeToText = new Dictionary<int, string>();
var hashCodeToCollision = new Dictionary<int, Tuple<string, string>>();

StringGenerator MakeGenerator(int stringLength) =>
    StringGenerator.Create(printables, buffer, stringLength);

// iterate over possible sizes, ascending ...
for (var i=1; i<=maxStringLength; i++)
{
    var generator = MakeGenerator(i);

    // iterate over all strings with that size
    while (generator.HasNext())
    {
        var text = generator.Next();
        var hashCode = text.GetHashCode();

        // check if a collision already occured for that hash code
        if (hashCodeToCollision.TryGetValue(hashCode, out var collision))
        {
            FoundIt(collision, text);
            return;
        }        
        
        // check if that hash code was already seen
        if (hashCodeToText.TryGetValue(hashCode, out var first))
        {
            hashCodeToCollision.Add(hashCode, Tuple.Create(first, text));
        }
        else
        {
            hashCodeToText.Add(hashCode, text);
        }
    }
}

Console.WriteLine("FAILED !");
return;

static void FoundIt(Tuple<string, string> collision, string third)
{
    var stringA = collision.Item1;
    var stringB = collision.Item2;
    var stringC = third;

    PrintWithHashCode(stringA);
    PrintWithHashCode(stringB);
    PrintWithHashCode(stringC);
}

static void PrintWithHashCode(string text) =>
    Console.WriteLine(text + "\t" + text.GetHashCode());

public abstract class StringGenerator
{
    public static StringGenerator Create(char[] printableChars, char[] buffer, int size) =>
        size == 0 
            ? CreateHead()
            : CreateTail(printableChars, buffer, size);

    private static StringGenerator CreateHead() => new HeadGenerator();

    private static StringGenerator CreateTail(char[] printableChars, char[] buffer, int size) =>
        new TailGenerator(printableChars, buffer, size);

    public abstract bool HasNext();

    public abstract string Next();
}

public class HeadGenerator : StringGenerator
{
    private bool invoked = false;

    public override bool HasNext() => !invoked;

    public override string Next() 
    {
        this.invoked = true;

        return string.Empty;
    }
}

public class TailGenerator : StringGenerator
{
    private readonly char[] printableChars;
    private readonly char[] buffer;
    private readonly int size;

    private StringGenerator prefixGenerator;
    private int printableCharIndex;

    public TailGenerator(char[] printableChars, char[] buffer, int size)
    {
        this.printableChars = printableChars;
        this.buffer = buffer;
        this.size = size;

        this.printableCharIndex = printableChars.Length;
        this.prefixGenerator = StringGenerator.Create(printableChars, buffer, size - 1);
    }

    public override bool HasNext() =>
        printableCharIndex < printableChars.Length || 
        prefixGenerator.HasNext();

    public override string Next()
    {
        if (printableCharIndex == printableChars.Length) 
        {
            prefixGenerator.Next();
            printableCharIndex = 0;
        }

        buffer[size - 1] = printableChars[printableCharIndex++];

        return new string(buffer, 0, size);
    }
}